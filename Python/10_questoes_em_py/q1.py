n = int(input())
solucaoVet = []
dificuldadeVet = []

for i in range(n):
    _, solucao, dificuldade = input().split()
    solucaoVet.append(solucao)
    dificuldadeVet.append(int(dificuldade))
    
imprimir = ''

for dificuldade in range(10,0, -1):
    for i in range(len(dificuldadeVet)):
        if(dificuldade == dificuldadeVet[i]):
            imprimir += solucaoVet[i]

print(imprimir)