# verifica se nome ja existe, se existe, substitui a marmita dele
def nomeJaExiste(nomeBuscar, marmitaNova, depita):
    for deposito in depita:
        if(deposito[0] == nomeBuscar):
            deposito[1] = marmitaNova
            return True
    return False


n = int(input())

depita = []

for i in range(n):
    nome, _, marmita = input().partition(' ')
    if(nomeJaExiste(nome, marmita, depita) == False):
        depita.append([nome, marmita])

print(len(depita))

depita.sort()
    
for nome, marmita in depita:
    print(marmita)
    