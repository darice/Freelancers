def funcaoAckermann(x, y):
    if(x == 0):
        return y+1
    elif(y == 0):
        return funcaoAckermann(x-1, 1)    
    else:
        return funcaoAckermann(x - 1, funcaoAckermann(x, y-1))
    

x, y = input().split()
x = int(x)
y = int(y)

print(funcaoAckermann(x, y))
