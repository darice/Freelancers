def ehLuta(idBuscar, lutas):
    for luta in lutas:
        if(luta[0] == idBuscar):
            return True
    return False

n = int(input())

for i in range(n):
    _ = input()

idLuta = ''
finalizar = 'FINISHHIM'
lutas = []

while(idLuta != finalizar):
    entrada = input()
    try:
        idLuta, _, _, ganhador = entrada.split()
    except:
        idLuta, lutaFinal = entrada.split()

    if(idLuta != finalizar):
        lutas.append([int(idLuta), int(ganhador)])

lutas.sort()

for luta1 in lutas:
    if(ehLuta(luta1[1], lutas) == True ):
        for luta2 in lutas:
            if(luta2[0] == luta1[1]):
                luta1[1] = luta2[1]
                break

vencedor = lutas[len(lutas)-1][1]

quantidadePartidasDoVencedor = 0
for luta in lutas:
    if(luta[1] == vencedor):
        quantidadePartidasDoVencedor += 1

print(quantidadePartidasDoVencedor)