#include <stdio.h>

void main()
{
    int A[3][4];

    printf("Preencha a matriz A:\n");

    for (int linha = 0; linha < 3; linha++)
    {
        for (int coluna = 0; coluna < 4; coluna++)
        {
            printf("A[%d][%d] = ", linha, coluna);
            scanf("%d", &A[linha][coluna]);
        }
    }

    int T[4][3];
    
    for (int linha = 0; linha < 3; linha++)
    {
        for (int coluna = 0; coluna < 4; coluna++)
        {
            T[coluna][linha] = A[linha][coluna];
        }
    }
    printf("\n");

    for (int linha = 0; linha < 3; linha++)
    {
        for (int coluna = 0; coluna < 4; coluna++)
        {
            printf("%d ", A[linha][coluna]);
        }
        printf("\n");
    }

    printf("\n");

    for (int linha = 0; linha < 4; linha++)
    {
        for (int coluna = 0; coluna < 3; coluna++)
        {
            printf("%d ", T[linha][coluna]);
        }
        printf("\n");
    }

}