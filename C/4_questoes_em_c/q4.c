#include <stdio.h>

float funcao(float r1, float r2, float v_in){
    float v_out = (r2*v_in) / (r1+r2);
    return v_out;
}

void main(){
    float r1, r2, v_in;
    printf("Digite o valor de r1: ");
    scanf("%f", &r1);
    printf("Digite o valor de r2: ");
    scanf("%f", &r2);
    printf("Digite o valor de v_in: ");
    scanf("%f", &v_in);
    
    printf("v_out = %.2f\n", funcao(r1, r2, v_in));
}