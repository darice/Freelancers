#include <stdio.h>

void main()
{
    int n;
    printf("Digite um número: ");
    scanf("%d", &n);
    if (n > 20)
    {
        printf("Número inválido\n");
    }
    else
    {
        if (n % 2 == 0 && n != 2)
        {
            printf("Não é primo\n");
        }
        else if (n % 3 == 0 && n != 3)
        {
            printf("Não é primo\n");
        }
        else if (n % 5 == 0 && n != 5)
        {
            printf("Não é primo\n");
        }
        else if (n % 7 == 0 && n != 7)
        {
            printf("Não é primo\n");
        }
        else if (n % 11 == 0 && n != 11)
        {
            printf("Não é primo\n");
        }
        else if (n % 13 == 0 && n != 13)
        {
            printf("Não é primo\n");
        }
        else if (n % 17 == 0 && n != 17)
        {
            printf("Não é primo\n");
        }
        else if (n % 19 == 0 && n != 19)
        {
            printf("Não é primo\n");
        }
        else
        {
            printf("É primo\n");
        }
    }
}
