#include <stdio.h>

void main(){
    float r1, r2, v_in, v_out;

    printf("Digite o valor de r1: ");
    scanf("%f", &r1);
    printf("Digite o valor de r2: ");
    scanf("%f", &r2);
    printf("Digite o valor de v_in: ");
    scanf("%f", &v_in);
    v_out = (r2*v_in) / (r1+r2);
    printf("v_out = %.2f\n", v_out);
}