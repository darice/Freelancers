//Obs: Como na questão não foi informado o tamanho do vetor, pedi para que o usuário digitasse.

#include <stdio.h>

int main()
{
    int quantidadeAlunos, i;
    //
    //leio a quantidade de entradas que o úsuario irá realizar
    printf("Digite a quantidade de alunos que deseja cadastrar: ");
    scanf("%d", &quantidadeAlunos);

    //nome é uma string de até 50 caracteres
    char nome[quantidadeAlunos][50];
    float nota[quantidadeAlunos];

    //percorro o laço n vezes, onde n corresponde a quantidade de alunos
    //nesse laço leio o nome e nota de cada aluno e armazeno nos meus vetores
    for (i = 0; i < quantidadeAlunos; i++)
    {
        printf("\nDigite o nome do aluno %d: ", i + 1);
        scanf("%s", nome[i]);
        printf("Digite a nota do aluno %d: ", i + 1);
        scanf("%f", &nota[i]);
    }

    //declaro a variavel soma que irá receber a soma de todas as notas dos alunos.
    float soma = 0;
    //percorro o laço n vezes, onde n corresponde a quantidade de alunos
    for (i = 0; i < quantidadeAlunos; i++)
    {
        //a variavel soma recebe a soma da nota do aluno de posição i com o valor anterior de soma
        soma += nota[i];
    }
    //a media é calculada pela soma das notas, dividido pela quantidade de alunos
    float media = soma / quantidadeAlunos;

    //A váriavel quantidadeMaiorQueMedia irá retornar a quantidade de alunos que obtiveram nota maior que a media
    int quantidadeMaiorQueMedia = 0;
    //percorro o laço n vezes, onde n corresponde a quantidade de alunos
    for (i = 0; i < quantidadeAlunos; i++)
    {
        //se a nota do aluno i for maior que a media
        if (nota[i] > media)
        {
            // então quantidadeMaiorQueMedia recebe a soma do valor anterior de quantidadeMaiorQueMedia com 1
            quantidadeMaiorQueMedia++;
        }
    }

    printf("\nNota media da turma: %.2f\n", media);
    printf("Quantidade de alunos que obtiveram nota maior que a media: %d\n", quantidadeMaiorQueMedia);
}