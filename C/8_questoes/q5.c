//Obs: Como na questão nao foi informado o preco do litro do combustivel, pedi para que o usuário digitasse.
//nao ficou claro que diferenca de custo a questão pede, então optei por realizar a diferenca de custo de cada modelo para o modelo mais economico

#include <stdio.h>

//declaro a constante n
//n corresponde ao tamanho do vetor;
#define n 10

int main()
{

	char modelo[n][20];
	float consumo[n];
	int maisEconomico = 0;
	int posicaoMaisEconomico;
	int i;

	//leio o preco por litro
	float precoLitro;
	printf("Digite o preço do litro do combustivel: ");
	scanf("%f", &precoLitro);

	//preencho meus vetores com os valores digitados pelo usuario
	for (i = 0; i < n; i++)
	{
		printf("\nDigite o modelo: ");
		scanf("%s", modelo[i]);
		printf("Digite o consumo(km/l): ");
		scanf("%f", &consumo[i]);
	}

	for (i = 0; i < n; i++)
	{
		//verifico se o valor do consumo do carro na posicao é mais economico do que maisEconomico
		//note que quanto maior o valor de "consumo", mais economico o carro é, pois roda mais quilometros com apenas 1 litro
		//tambem entro nesse if, quando i=0, para que maisEconomico receba um valor
		if (consumo[i] > maisEconomico | i == 0)
		{
			//maisEconomico recebe um valor mais baixo que ele próprio
			maisEconomico = consumo[i];
			//guardo a posicao do carro na variavel posicaoMaisEconomico
			posicaoMaisEconomico = i;
		}
	}

	//A variavel custo1500MaisEconomico recebe o custo para percorrer 1500km do modelo mais economico
	//O custo para percorrer 1500km é calculado pelo produto do preço do litro pelo consumo para percorrer 1500km
	float custo1500MaisEconomico = (1500 / consumo[posicaoMaisEconomico]) * precoLitro;

	for (i = 0; i < n; i++)
	{
		printf("\nModelo: %s\n", modelo[i]);
		//O consumo para percorrer 1500km é calculado pela divisão de 1500 pelo consumo do carro
		float consumo1500 = 1500 / consumo[i];
		printf("Consumo de litros para percorrer 1500 km: %.2f\n", consumo1500);
		//O custo para percorrer 1500km é calculado pelo produto do preço do litro pelo consumo para percorrer 1500km
		float custo1500 = precoLitro * consumo1500;
		printf("Custo para percorrer 1500 km: R$ %.2f\n", custo1500);

		//se o carro nao for o mais economico, então calcula a diferenca
		if (i != posicaoMaisEconomico)
		{
		//diferenca do custo do modelo da posicao i para o custo do modelo mais economico
			printf("Diferença de custo para o mais economico: R$ %.2f\n", custo1500 - custo1500MaisEconomico);
		}
		//se for o mais economico, imprime que é o mais economico
		else
		{
			printf("Este é o modelo mais econômico\n");
		}
	}
}
