//Obs: Nao ficou claro como devo efetuar a atualização do estoque,
//então, optei por solicitar que seja digitado a quantidade reposta no estoque
//quando o produto for atendido integralmente

#define n 10

#include <stdio.h>

int main()
{
    int i;
    //codigoProduto é um vetor que armazena o código de cada produto
    int codigoProduto[n];
    //quantidadeEstoque é um vetor que armazena o total dos produtos em estoque
    int quantidadeEstoque[n];

    //preenchendo os vetores com os valores digitados pelo usuario
    for (i = 0; i < n; i++)
    {
        printf("\nDigite o código do produto: ");
        scanf("%d", &codigoProduto[i]);
        printf("Digite a quantidade no estoque: ");
        scanf("%d", &quantidadeEstoque[i]);
    }

    //codigoCliente armazena o código do cliente
    int codigoCliente;
    //codigoProdutoCompra armazena o código do produto que o cliente deseja comprar
    int codigoProdutoCompra;
    //quantidadeCompra armazena a quantidade do produto que o cliente deseja comprar
    int quantidadeCompra;
    //posicaoCompra armazena a posiçao do vetor que se encontra o produto que o cliente deseja
    int posicaoCompra;
    do
    {
        //lendo o codigo do cliente
        printf("\nDigite o código do cliente: ");
        scanf("%d", &codigoCliente);
        //verifico se o codigoCliente é diferente de 0
        if (codigoCliente != 0)
        {
            //leio o codigo que o cliente deseja comprar
            printf("Digite o código do produto que deseja comprar: ");
            scanf("%d", &codigoProdutoCompra);
            //A variavel encontrado é util para saber se o codigo do produto solicitado existe
            //inicialmente encontrado recebe 0, quando, e se, o codigo for encontrado, encontrado recebe 1.
            int encontrado = 0;
            for (i = 0; i < n; i++)
            {
                if (codigoProdutoCompra == codigoProduto[i])
                {
                    //guardo a posicao que o produto se encontra no vetor
                    posicaoCompra = i;
                    encontrado = 1;
                    //break serve para encerrar o laço for
                    //já posso encerrar, visto que encontrei o produto
                    break;
                }
            }
            //se a o codigo nao foi encontrado, a variavel encontrado continuou com o valor recebido inicialemente, que foi 0
            if (encontrado == 0)
            {
                printf("Código inexistente\n");
            }
            //entra nesse else, somente se o codigo existir
            else
            {
                //leio a quantidade do produto que o cliente deseja comprar
                printf("Digite a quantidade que deseja comprar: ");
                scanf("%d", &quantidadeCompra);
                //se a quantidade for maior do que tem no estoque, não permito a compra
                if (quantidadeCompra > quantidadeEstoque[posicaoCompra])
                {
                    printf("Nao temos estoque suficiente dessa mercadoria\n");
                }
                //se a quantidade for menor ou igual ao que tem no estoque, a compra é permitida
                else if (quantidadeCompra <= quantidadeEstoque[posicaoCompra])
                {
                    //da quantidade no estoque, subtraio a quantida vendida.
                    quantidadeEstoque[posicaoCompra] = quantidadeEstoque[posicaoCompra] - quantidadeCompra;
                    printf("Pedido atendido. Obrigado e volte sempre.\n");

                    //se meu estoque tiver sido zerado, peço para que reponham
                    if (quantidadeEstoque[posicaoCompra] == 0)
                    {
                        //leio a nova quantidade reposta no estoque
                        printf("O produto %d foi atendido integralmente. É necessário repor o estoque\nDigite a nova quantidade no estoque: ", codigoProduto[posicaoCompra]);
                        scanf("%d", &quantidadeEstoque[posicaoCompra]);
                    }
                }
            }
        }
    } while (codigoCliente != 0);
    //saio do while somente se o código do cliente for igual a 0

    //imprimo os códigos dos produtos com seus respectivos estoques já atualizados.
    for (i = 0; i < n; i++)
    {
        printf("\n\nCodigo do produto: %d", codigoProduto[i]);
        printf("\nQuantidade no estoque: %d", quantidadeEstoque[i]);
    }
}