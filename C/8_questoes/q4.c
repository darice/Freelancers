//OBS: Não ficou muito claro na hora de imprimir os destalhes da correção de cada questão
//se como resposta deveria ser informado a resposta do usuario ou resposta correta
//optei pela resposta do usuario

#include <stdio.h>

int main()
{
    int i, corretas = 0, erradas = 0;
    //declaro os vetor de caracteres de 11 posições
    //é necessário declarar com uma posição a mais, pois a última posição será preenchida com '\0'
    char gabarito[11], cartaoResposta[11];

    //preenchendo o vetor gabarito com os valores digitados pelo usuario
    printf("Digite a sequencia de respostas corretas (sem espaço entre as letras): ");
    scanf("%s", gabarito);

    //preenchendo o vetor cartaoResposta com os valores digitados pelo usuario
    printf("Digite a sequencia de respostas do cartão resposta (sem espaço entre as letras): ");
    scanf("%s", cartaoResposta);

    for (i = 0; i < 10; i++)
    {
        printf("Q%d - Resposta: ", i + 1);
        if(gabarito[i] == cartaoResposta[i])
        {
            printf("| Correta\n");
            corretas ++;
        }
        else
        {
            printf("| Incorreta\n");
            erradas++;
        }
    }

printf("Quantidade de questoes corretas: %d\n", corretas);
printf("Quantidade de questoes erradas: %d\n", erradas);



}