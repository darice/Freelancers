//Obs: Como na questão não foi informado o tamanho do vetor, pedi para que o usuário digitasse.

#include <stdio.h>

int main()
{
    int i, j, aux, tamanhoVetor;

    //leio a quantidade de entradas que o úsuario irá realizar
    printf("Digite a quantidade de numeros que o vetor terá: ");
    scanf("%d", &tamanhoVetor);

    //declaro o vetor de tamanho "tamanhoVetor"
    int vetor[tamanhoVetor];

    //preenchendo o vetor com os valores digitados pelo usuario
    for (i = 0; i < tamanhoVetor; i++)
    {
        printf("Vetor[%d] = ", i);
        scanf("%d", &vetor[i]);
    }

    //ordeno o valor de forma decrescente
    //o laço se repete tamanhoVetor-1 vezes
    for (i = 1; i < tamanhoVetor; i++)
    {
        for (j = 0; j < tamanhoVetor - 1; j++)
        {
            //se o vetor de determinada posicao, tiver um valor menor que o vetor da posicao seguinte a ele
            if (vetor[j] < vetor[j + 1])
            {
                //troco o valor das posicoes dos vetores
                aux = vetor[j];
                vetor[j] = vetor[j + 1];
                vetor[j + 1] = aux;
            }
        }
    }

    //imprimi o vetor
    for (i = 0; i < tamanhoVetor; i++)
    {
        printf("%d, ", vetor[i]);
    }
}