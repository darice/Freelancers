#include <stdio.h>

int main()
{
    int i, j, aux, tamanhoVetor;

    //leio a quantidade de entradas que o úsuario irá realizar
    printf("Digite a quantidade de numeros que o vetor terá: ");
    scanf("%d", &tamanhoVetor);

    //declaro o vetor de tamanho "tamanhoVetor"
    float vetor[tamanhoVetor];

    //preenchendo o vetor com os valores digitados pelo usuario
    for (i = 0; i < tamanhoVetor; i++)
    {
        printf("Vetor[%d] = ", i);
        scanf("%f", &vetor[i]);
    }

    float maiorValor = 0;
    float menorValor = 0;
    float soma = 0;
    for (i = 0; i < tamanhoVetor; i++)
    {
        if (vetor[i] > maiorValor | i == 0)
        {
            maiorValor = vetor[i];
        }
        if (vetor[i] < menorValor | i == 0)
        {
            menorValor = vetor[i];
        }
        soma += vetor[i];
    }
    float media = soma / tamanhoVetor;

    int opcao;
    do
    {
        printf("\n\n*************************Menu**************************\n");
        printf("\n 1 – Mostrar maior elemento do vetor;\n");
        printf(" 2 – Mostrar o menor elemento do vetor;\n");
        printf(" 3 – Mostrar a Média;\n");
        printf(" 4 – Mostrar números inseridos;\n");
        printf(" 0 – Encerrar;\n");
        printf("\nDigite o numero corresponde a opcao que deseja: ");
        scanf("%d", &opcao);
        switch (opcao)
        {
        case 1:
            printf("\nMaior elemento do vetor: %.2f", maiorValor);
            break;
        case 2:
            printf("\nMenor elemento do vetor: %.2f", menorValor);
            break;
        case 3:
            printf("\nMedia: %.2f", media);
            break;
        case 4:
            //ordeno o vetor
            //o laço se repete tamanhoVetor-1 vezes
            for (i = 1; i < tamanhoVetor; i++)
            {
                //o laço se repete tamanhoVetor-1 vezes
                for (j = 0; j < tamanhoVetor - 1; j++)
                {
                    //se o vetor de determinada posicao tiver um valor maior que o vetor da posicao seguinte a ele
                    if (vetor[j] > vetor[j + 1])
                    {
                        //troco os valores das posicoes do vetor
                        aux = vetor[j];
                        vetor[j] = vetor[j + 1];
                        vetor[j + 1] = aux;
                    }
                }
            }
            //imprimo o vetor
            for (i = 0; i < tamanhoVetor; i++)
            {
                printf("%.2f\n", vetor[i]);
            }
            break;
        case 0:
            printf("Finalizado com sucesso!");
            break;
        default:
            break;
        }
    } while (opcao != 0);
}