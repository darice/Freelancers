//Obs: Como na questão não foi informado o tamanho do vetor, pedi para que o usuário digitasse.
#include <stdio.h>

int main()
{
    int i, tamanhoVetor, tamanhoVetorPar = 0, tamanhoVetorImpar = 0;

    //leio a quantidade de entradas que o úsuario irá realizar
    printf("Digite a quantidade de numeros que o vetor terá: ");
    scanf("%d", &tamanhoVetor);

    int vetor[tamanhoVetor];
    int par[tamanhoVetor];
    int impar[tamanhoVetor];

    //preenchendo o vetor com os valores digitados pelo usuario
    for (i = 0; i < tamanhoVetor; i++)
    {
        printf("Vetor[%d] = ", i);
        scanf("%d", &vetor[i]);
    }

    //preencho os vetores par e impar
    for (i = 0; i < tamanhoVetor; i++)
    {
        //se o indice de vetor, quando dividido por 2, retornar 0
        if (i % 2 == 0)
        {
            //adiciono o valor no vetor par
            par[tamanhoVetorPar] = vetor[i];
            //somo um no tamanho do vetor
            tamanhoVetorPar++;
        }
        //se o indice de vetor, quando dividido por 2, retornar 1
        else
        { //adiciono o valor no vetor impar
            impar[tamanhoVetorImpar] = vetor[i];
            //somo um no tamanho do vetor
            tamanhoVetorImpar++;
        }
    }

    //imprimo o vetor par
    for (i = 0; i < tamanhoVetorPar; i++)
    {
        printf("%d, ", par[i]);
    }
    //imprimo o vetor impar
    printf("\n");
    for (i = 0; i < tamanhoVetorImpar; i++)
    {
        printf("%d, ", impar[i]);
    }
}