//importação das biblioteca
#include <stdio.h>   //biblioteca das funções scanf, printf...
#include <stdlib.h>  //biblioteca das funções atoi, rand...
#include <string.h>  //biblioteca de funções para trabalhar com strings
#include <stdbool.h> //biblioteca para trabalhar com o tipo bolenano
#include <time.h>    //biblioteca para trabalhar com números aleátorios

//declaração das variáveis
struct Lote
{
    int codigo;
    char nome[11];
    int quantidadeBarris;
    float precoCompraUnidade;
    float precoVendaUnidade;
    int codigoFornecedor;
};
struct Fornecedor
{
    int codigo;
    char nome[10];
};

//declaração das funções do menu
void cadastrarLoteMenu(int *tamanhoLotePont, struct Lote *listaLote, int *tamanhoFornecedorPont, struct Fornecedor *listaFornecedor);
void excluirLoteMenu(struct Lote *listaLote, int *tamanhoPont);
void alterarLoteMenu(int tamanhoLote, struct Lote *listaLote, int *tamanhoFornecedorPont, struct Fornecedor *listaFornecedor);
void cadastrarFornecedorMenu(struct Fornecedor *listaFornecedor, int *tamanhoPont);
void pesquisarLoteMenu(struct Lote *listaLote, int tamanho);
void pesquisarFornecedorMenu(struct Fornecedor *listaFornecedor, int tamanhoFornecedor, struct Lote *listaLote, int tamanhoLote);
void relatorioLucratividadeMenu(struct Fornecedor *listaFornecedor, int tamanhoFornecedor, struct Lote *listaLote, int tamanhoLote);
void relatorioLoteMenu(int tamanho, struct Lote *listaLote);

//declaracao de funções auxiliares:

//funções para realizar a leitura de uma variável.
//Elas retornam o número lido, somente quando este passar pelas validações.
int lerInteiro();
int lerOpcao(char max);
float lerPreco();
float lerPrecoVendaUnidade(float precoCompraUnidade);
int lerQuantidadeBarris();

// funções de ornação pelo metódo da bolha
void ordenarListaLote(struct Lote *listaLote, int tamanho);
void ordenarListaFornecedor(struct Fornecedor *listaFornecedor, int tamanho);

//funções que realizam uma busca binária por um determinado codigo
//se o codigo for encontrado, retornam sua posição no vetor
//se nao for encontrado, retorna -1
int buscarLote(int codigoBuscar, struct Lote *listaLote, int tamanho);
int buscarFornecedor(int codigoBuscar, struct Fornecedor *listaFornecedor, int tamanho);

//funções para realizar o cadastro automático e aleátorio dos 10 primeiros lotes
void cadastroAutomatico(struct Lote *listaLote, int *tamanhoLotePont, struct Fornecedor *listaFornecedor, int *tamanhoFornecedorPont);
void stringAleatoria(char *str);

//essa função imprime um lote de determina posição
void imprimirLote(int posicao, struct Lote *listaLote);

//funções

int main()
{
    struct Lote listaLote[80];
    int tamanhoListaLote = 0;

    struct Fornecedor listaFornecedor[100];
    int tamanhoListaFornecedor = 0;

    cadastroAutomatico(listaLote, &tamanhoListaLote, listaFornecedor, &tamanhoListaFornecedor);

    do
    {
        printf("\n\n\n\n\n");
        printf("**Bem vindo!!!**\n\n");
        printf("Digite uma das opcoes para continuar:\n");
        printf("\t1- Cadastrar novo lote de barris\n");
        printf("\t2- Excluir um lote cadastrado\n");
        printf("\t3- Atualizar lote cadastrado\n");
        printf("\t4- Cadastrar novo fornecedor\n");
        printf("\t5- Pesquisar por lote\n");
        printf("\t6- Pesquisar por fornecedor\n");
        printf("\t7- Relatorio de lucratividade\n");
        printf("\t8- Relatorio com todos os lotes cadastrados\n");
        printf("\t9- Sair\n\t\t");
        int opcao;
        opcao = lerOpcao('9');
        printf("\n\n\n\n\n");
        switch (opcao)
        {
        case 1:
            if (tamanhoListaLote != 80)
            {
                cadastrarLoteMenu(&tamanhoListaLote, listaLote, &tamanhoListaFornecedor, listaFornecedor);
            }
            else
            {
                printf("Voce ja cadastrou o limite de lotes permitidos");
                printf("**Pressione enter para retornar ao menu**");
                getchar();
            }
            break;
        case 2:
            excluirLoteMenu(listaLote, &tamanhoListaLote);
            break;
        case 3:
            alterarLoteMenu(tamanhoListaLote, listaLote, &tamanhoListaFornecedor, listaFornecedor);
            break;
        case 4:
            cadastrarFornecedorMenu(listaFornecedor, &tamanhoListaFornecedor);
            break;
        case 5:
            pesquisarLoteMenu(listaLote, tamanhoListaLote);
            break;
        case 6:
            pesquisarFornecedorMenu(listaFornecedor, tamanhoListaFornecedor, listaLote, tamanhoListaLote);
            break;
        case 7:
            relatorioLucratividadeMenu(listaFornecedor, tamanhoListaFornecedor, listaLote, tamanhoListaLote);
            break;
        case 8:
            relatorioLoteMenu(tamanhoListaLote, listaLote);
            break;
        case 9:
            printf("Sua sessao foi encerrada com sucesso!\n");
            return 0;
        }
    } while (true); //o laço se repete infinitamante, exceto qnd o usuário digita 9.
}

//realiza o cadastro de um novo lote de barris
void cadastrarLoteMenu(int *tamanhoLotePont, struct Lote *listaLote, int *tamanhoFornecedorPont, struct Fornecedor *listaFornecedor)
{
    printf("**Cadastro de lote**\n\n");
    printf("Digite o codigo: ");
    int codigoLote = lerInteiro();
    if (buscarLote(codigoLote, listaLote, *tamanhoLotePont) != -1)
    //verifica se ha um lote cadastrado com esse codigo
    {
        printf("Ja existe um lote cadastrado com esse codigo\n");
    }
    else
    //cadastra um novo lote
    {
        listaLote[*tamanhoLotePont].codigo = codigoLote;
        printf("\nDigite o nome do tipo: ");
        scanf("%s", listaLote[*tamanhoLotePont].nome);
        printf("\nDigite a quantidade de barris comprados: ");
        listaLote[*tamanhoLotePont].quantidadeBarris = lerQuantidadeBarris();
        printf("\nDigite o preco de compra do barril por unidade: ");
        listaLote[*tamanhoLotePont].precoCompraUnidade = lerPreco();
        printf("\nDigite o preco de venda do barril por unidade: ");
        listaLote[*tamanhoLotePont].precoVendaUnidade = lerPrecoVendaUnidade(listaLote[*tamanhoLotePont].precoCompraUnidade);
        printf("\nDigite o codigo do fornecedor: ");
        int codigoFornecedor = lerInteiro();
        listaLote[*tamanhoLotePont].codigoFornecedor = codigoFornecedor;
        //verifica se ha um fornecedor cadastrado com esse codigo
        if (buscarFornecedor(codigoFornecedor, listaFornecedor, *tamanhoFornecedorPont) == -1)
        {
            //cadastra o novo fornecedor
            printf("Nao ha nenhum fornecedor com o codigo digitado. Para cadastra-lo digite o nome da plataforma: ");
            listaFornecedor[*tamanhoFornecedorPont].codigo = codigoFornecedor;
            setbuf(stdin, NULL);
            scanf("%[^\n]s", listaFornecedor[*tamanhoFornecedorPont].nome);
            (*tamanhoFornecedorPont)++;
        }
        (*tamanhoLotePont)++;
        printf("\nLote cadastrado com sucesso!\n");
    }
    getchar();
    printf("**Pressione enter para retornar ao menu**");
    getchar();
}

//exclui um lote cadastrado
void excluirLoteMenu(struct Lote *listaLote, int *tamanhoPont)
{
    printf("Digite o codigo do produto que deseja buscar: ");
    int codigo = lerInteiro();
    int posicao = buscarLote(codigo, listaLote, *tamanhoPont);
    //se a função buscarlote retornar -1, então, nao ha lote cadastrado com esse codigo.
    if (posicao == -1)
    {
        printf("\nNao ha nenhum lote cadastrado com este codigo\n");
    }
    else
    {
        (*tamanhoPont)--; //impesso a visualização da última posição do meu vetor
        int i;
        for (i = posicao; i < *tamanhoPont; i++)
        {
            //Movo uma posição para cima todo lote que se encontra abaixo da posição que vou excluir
            listaLote[i].codigo = listaLote[i + 1].codigo;
            strcpy(listaLote[i].nome, listaLote[i + 1].nome);
            listaLote[i].precoCompraUnidade = listaLote[i + 1].precoCompraUnidade;
            listaLote[i].precoVendaUnidade = listaLote[i + 1].precoVendaUnidade;
            listaLote[i].codigoFornecedor = listaLote[i + 1].codigoFornecedor;
        }
        printf("Lote excluido com sucesso!\n");
    }
    printf("**Pressione enter para retornar ao menu**\n");
    getchar();
    getchar();
}

//função para alterar as informações cadastradas de um lote
void alterarLoteMenu(int tamanhoLote, struct Lote *listaLote, int *tamanhoFornecedorPont, struct Fornecedor *listaFornecedor)
{

    printf("**Alterar lote cadastrado**\n\n");
    printf("Digite o codigo do lote que deseja alterar: ");
    int codigoLote = lerInteiro();
    int posicao = buscarLote(codigoLote, listaLote, tamanhoLote);
    if (posicao == -1)
    {
        printf("Nao ha nenhum lote cadastrado com esse codigo\n");
    }
    else
    {
        //substituo os valores antigos pelos novos digitados pelo usúario
        printf("Nome anterior do tipo: \"%s\". Digite o novo nome: ", listaLote[posicao].nome);
        scanf("%s", listaLote[posicao].nome);
        printf("\nQuantidade anterior de barris comprados: \"%d\". Digite a nova quantidade: ", listaLote[posicao].quantidadeBarris);
        listaLote[posicao].quantidadeBarris = lerQuantidadeBarris();
        printf("\nPreco anterior da compra do barril por unidade: \"R$ %.2f\". Digite o novo preço: ", listaLote[posicao].precoCompraUnidade);
        listaLote[posicao].precoCompraUnidade = lerPreco();
        printf("\nPreco anterior da venda do barril por unidade: \"R$ %.2f\". Digite o novo preço: ", listaLote[posicao].precoCompraUnidade);
        listaLote[posicao].precoVendaUnidade = lerPrecoVendaUnidade(listaLote[posicao].precoCompraUnidade);
        printf("\nCodigo anterior do fornecedor: %d. Digite outro codigo: ", listaLote[posicao].codigoFornecedor);
        int codigoFornecedor = lerInteiro();
        listaLote[posicao].codigoFornecedor = codigoFornecedor;
        //se o usuário digitar um codigo do Fornecedor, que ainda nao foi cadastrado, ele é cadastrado
        if (buscarFornecedor(codigoFornecedor, listaFornecedor, *tamanhoFornecedorPont) == -1)
        {
            printf("Nao ha nenhum fornecedor com o codigo digitado. Para cadastra-lo digite o nome da plataforma: ");
            listaFornecedor[*tamanhoFornecedorPont].codigo = codigoFornecedor;
            setbuf(stdin, NULL);
            scanf("%[^\n]s", listaFornecedor[*tamanhoFornecedorPont].nome);
            (*tamanhoFornecedorPont)++;
        }
        printf("\nLote alterado com sucesso!\n");
    }

    printf("**Pressione enter para retornar ao menu**");
    getchar();
    getchar();
}

//funcao para cadastrar um novo fornecedor
void cadastrarFornecedorMenu(struct Fornecedor *listaFornecedor, int *tamanhoPont)
{

    printf("**Cadastro de fornecedor**\n\n");
    printf("Digite o codigo: ");
    int codigo = lerInteiro();
    if (buscarFornecedor(codigo, listaFornecedor, *tamanhoPont) != -1)
    {
        printf("Ja existe um lote cadastrado com esse codigo\n");
    }
    else
    {
        //cadastra um novo fornecedor
        listaFornecedor[*tamanhoPont].codigo = codigo;
        printf("\nDigite o nome da plataforma: ");
        setbuf(stdin, NULL);
        scanf("%[^\n]s", listaFornecedor[*tamanhoPont].nome);
        (*tamanhoPont)++;
        printf("\nFornecedor cadastrado com sucesso!\n");
    }
    printf("**Pressione enter para voltar**");
    getchar();
    getchar();
}

//pesquisa o codigo digitado pelo usúario e retorna o lote correspondente
void pesquisarLoteMenu(struct Lote *listaLote, int tamanho)
{
    printf("Digite o codigo do lote que deseja buscar: ");
    int codigo = lerInteiro();
    int posicao = buscarLote(codigo, listaLote, tamanho);
    if (posicao == -1)
    {
        printf("Nao ha nenhum lote cadastrado com esse codigo\n");
    }
    else
    {
        imprimirLote(posicao, listaLote);
    }
    printf("**Pressione enter para voltar**");
    getchar();
    getchar();
}

//pesquisa o codigo digitado pelo usúario e retorna o  fornecedor correspondente
void pesquisarFornecedorMenu(struct Fornecedor *listaFornecedor, int tamanhoFornecedor, struct Lote *listaLote, int tamanhoLote)
{
    printf("Digite o codigo do fornecedor que deseja buscar: ");
    int codigo = lerInteiro();
    int posicao = buscarFornecedor(codigo, listaFornecedor, tamanhoFornecedor);
    if (posicao == -1)
    {
        printf("Nao ha nenhum fornecedor cadastrado com esse codigo\n");
    }
    else
    {
        printf("\ncodigo: %d\n", listaFornecedor[posicao].codigo);
        printf("Nome da plataforma: %s\n", listaFornecedor[posicao].nome);
        float totalCompra = 0; //total comprado desse fornecedor
        float totalVenda = 0;  //total obtido com as vendas desse fornecedor
        int totalLotes = 0;    //total de lotes cadastrados comprados desse fornecedor
        int i;
        for (i = 0; i < tamanhoLote; i++)
        {
            if (listaLote[i].codigoFornecedor == listaFornecedor[posicao].codigo)
            {
                totalLotes++;
                totalCompra += (listaLote[i].precoCompraUnidade * listaLote[i].quantidadeBarris);
                totalVenda += (listaLote[i].precoVendaUnidade * listaLote[i].quantidadeBarris);
            }
        }
        printf("Total de lotes cadastrados desse fornecedor: %d\n", totalLotes);
        printf("Total comprado desse fornecedor: R$ %.2f\n", totalCompra);
        printf("Total obtido com as vendas desse fornecedor: R$ %.2f\n", totalVenda);
        printf("Lucro liquido total: R$ %.2f\n", totalVenda - totalCompra);
    }
    printf("\n**Pressione enter para voltar**");
    getchar();
    getchar();
}

//imprime o lucro por codigo cadastrado
void relatorioLucratividadeMenu(struct Fornecedor *listaFornecedor, int tamanhoFornecedor, struct Lote *listaLote, int tamanhoLote)
{
    printf("**Relatorio de Lucratividade**\n\n");
    int i;
    for (i = 0; i < tamanhoLote; i++)
    {
        printf("Codigo: %d\n", listaLote[i].codigo);
        printf("Nome do tipo: %s\n", listaLote[i].nome);
        printf("Codigo do fornecedor: %d\n", listaLote[i].codigoFornecedor);
        int posicaoFornecedor = buscarFornecedor(listaLote[i].codigo, listaFornecedor, tamanhoFornecedor);
        //faço o produto da quantidade de barris pelo preço de cada barril, obtendo o preço total
        float compra = (listaLote[i].precoCompraUnidade * listaLote[i].quantidadeBarris);
        float venda = (listaLote[i].precoVendaUnidade * listaLote[i].quantidadeBarris);
        printf("Valor pago pelo lote de barris: R$ %.2f\n", compra);
        printf("Lucro bruto obtido com a venda: R$ %.2f\n", venda);
        printf("Lucro liquido: R$ %.2f\n\n", venda - compra);
    }
    if (tamanhoLote == 0)
    {
        printf("Voce nao cadastrou nenhum lote!\n");
    }
    printf("**Pressione enter para voltar**");
    getchar();
    getchar();
}

//imprime todos os lotes cadastrados
void relatorioLoteMenu(int tamanho, struct Lote *listaLote)
{
    ordenarListaLote(listaLote, tamanho);
    printf("**Relatorio dos lotes cadastrados**\n\n");
    int i;
    for (i = 0; i < tamanho; i++)
    {
        imprimirLote(i, listaLote);
    }
    if (tamanho == 0)
    {
        printf("Voce nao cadastrou nenhum lote!\n");
    }
    printf("**Pressione enter para retornar ao menu**\n");
    getchar();
    getchar();
}

int lerInteiro()
// Essa função se repete até que o usuário digite apenas números
//retorna o inteiro digitado
{
    do
    {
        char inteiro[20];
        int temCaractere = 0;
        scanf("%s", inteiro);
        int i;
        for (i = 0; inteiro[i] != '\0'; i++)
        {
            if (!(inteiro[i] >= '0' & inteiro[i] <= '9'))
            {
                temCaractere = 1;
                break;
            }
        }
        if (temCaractere == 0)
        {
            return atoi(inteiro); //converte string para inteiro e o retorna
        }
        printf("Apenas numeros! Digite novamente: ");
    } while (true);
}

int lerOpcao(char max)
// Essa função se repete até que o úsuario digite um número de um único algarismo entre 0 até max
// max é o maior valor disponíveis entre as opções que o usúario tem
{
    do
    {
        char opcao[20];
        scanf("%s", opcao);
        if ((opcao[1]) == '\0' & opcao[0] >= '1' & opcao[0] <= max)
        {
            return atoi(opcao); //converte string para inteiro e o retorna
        }
        printf("Opcao invalida. Digite novamente: ");
    } while (true);
}

//função para ler um float
//retorna o valor digitado, se este for válido
float lerPreco()
{
    do
    //O laço se repete até que o usuário digite um valor válido para o preço
    {
        char preco[25];
        int ponto = 0; //conta a quantidade de pontos que aparecem
        scanf("%s", preco);
        int i;
        for (i = 0; i < strlen(preco); i++)
        {
            if (preco[i] == ',') // converte vírgula para ponto
            {
                preco[i] = '.';
            }
            if (preco[i] == '.')
            {
                ponto++;
            }
            else if (!(preco[i] >= '0' & preco[i] <= '9')) //verifica se ha um caracter que nao seja um dígito ou um ponto
            {
                ponto = 2; // ponto recebe 2 apenas para nao passar pela última verificação
                break;
            }
            if (ponto == 2) //permite apenas um ponto no preço
            {
                break;
            }
        }
        if (ponto != 2)
        {
            return atof(preco);
        }
        printf("Valor invalido. Digite novamente: ");

    } while (true);
}

//essa função garante que o preço de compra nao seja maior que o preço de venda
float lerPrecoVendaUnidade(float precoCompraUnidade)
{
    float precoVendaUnidade = lerPreco();
    while (precoVendaUnidade < precoCompraUnidade)
    {
        printf("O valor deve ser maior ou igual ao da compra (R$ %.2f). Digite novamente: ", precoCompraUnidade);
        precoVendaUnidade = lerPreco();
    }
    return precoVendaUnidade;
}

// A função é composta por um laço que se repete até o usuário digitar um inteiro entre 50 e 100
int lerQuantidadeBarris()
{
    int quantidadeBarris = lerInteiro();
    while (quantidadeBarris < 50 || quantidadeBarris > 100)
    {
        printf("O valor deve estar entre 50 e 100. Digite novamente: ");
        quantidadeBarris = lerInteiro();
    }
    return quantidadeBarris;
}

//função para ordenar pelo metódo bolha os lotes cadastrados
void ordenarListaLote(struct Lote *listaLote, int tamanho)
{
    int i, j, auxInteiro;
    float auxPreco;
    char auxNome[20];
    for (i = 1; i < tamanho; i++)
    {
        for (j = 0; j < tamanho - 1; j++)
        {
            if (listaLote[j].codigo > listaLote[j + 1].codigo)
            {
                auxInteiro = listaLote[j].codigo;
                listaLote[j].codigo = listaLote[j + 1].codigo;
                listaLote[j + 1].codigo = auxInteiro;

                strcpy(auxNome, listaLote[j].nome);
                strcpy(listaLote[j].nome, listaLote[j + 1].nome);
                strcpy(listaLote[j + 1].nome, auxNome);

                auxInteiro = listaLote[j].quantidadeBarris;
                listaLote[j].quantidadeBarris = listaLote[j + 1].quantidadeBarris;
                listaLote[j + 1].quantidadeBarris = auxInteiro;

                auxPreco = listaLote[j].precoCompraUnidade;
                listaLote[j].precoCompraUnidade = listaLote[j + 1].precoCompraUnidade;
                listaLote[j + 1].precoCompraUnidade = auxPreco;

                auxPreco = listaLote[j].precoVendaUnidade;
                listaLote[j].precoVendaUnidade = listaLote[j + 1].precoVendaUnidade;
                listaLote[j + 1].precoVendaUnidade = auxPreco;

                auxInteiro = listaLote[j].codigoFornecedor;
                listaLote[j].codigoFornecedor = listaLote[j + 1].codigoFornecedor;
                listaLote[j + 1].codigoFornecedor = auxInteiro;
            }
        }
    }
}

//funcao para ordenar pelo metódo bolha os fornecedor cadastrados
void ordenarListaFornecedor(struct Fornecedor *listaFornecedor, int tamanho)
{
    int i, j, auxCodigo;
    char auxNome[20];
    for (i = 1; i < tamanho; i++)
    {
        for (j = 0; j < tamanho - 1; j++)
        {
            if (listaFornecedor[j].codigo > listaFornecedor[j + 1].codigo)
            {
                auxCodigo = listaFornecedor[j].codigo;
                listaFornecedor[j].codigo = listaFornecedor[j + 1].codigo;
                listaFornecedor[j + 1].codigo = auxCodigo;

                strcpy(auxNome, listaFornecedor[j].nome);
                strcpy(listaFornecedor[j].nome, listaFornecedor[j + 1].nome);
                strcpy(listaFornecedor[j + 1].nome, auxNome);
            }
        }
    }
}

//a função realiza uma busca binária nos codigos de lotes cadastrados
//procura pelo codigo recebido como parametro: codigoBuscar
int buscarLote(int codigoBuscar, struct Lote *listaLote, int tamanho)
{
    ordenarListaLote(listaLote, tamanho);
    int e = -1;
    while (e < tamanho - 1)
    {
        int m = (e + tamanho) / 2;
        if (listaLote[m].codigo < codigoBuscar)
            e = m;
        else
            tamanho = m;
    }
    if (listaLote[tamanho].codigo == codigoBuscar)
    {
        // se o codigo for encontrado entre os codigos de lotes cadastrados retorna sua posição
        return tamanho;
    }
    else
    {
        // se nao for encontrado, retorna -1
        return -1;
    }
}

//a função realiza uma busca binária nos codigos de fornecedores cadastrados
//procura pelo codigo recebido como parametro: codigoBuscar
int buscarFornecedor(int codigoBuscar, struct Fornecedor *listaFornecedor, int tamanho)
{
    ordenarListaFornecedor(listaFornecedor, tamanho);
    int e = -1;
    while (e < tamanho - 1)
    {
        int m = (e + tamanho) / 2;
        if (listaFornecedor[m].codigo < codigoBuscar)
            e = m;
        else
            tamanho = m;
    }
    if (listaFornecedor[tamanho].codigo == codigoBuscar)
    {
        // se o codigo for retorna sua posição
        return tamanho;
    }
    else
    {
        //se nao for encontrado, retorna -1
        return -1;
    }
}

//realiza o cadastro de 10 lotes
// a função é chamada assim que o programa é executada.
void cadastroAutomatico(struct Lote *listaLote, int *tamanhoLotePont, struct Fornecedor *listaFornecedor, int *tamanhoFornecedorPont)
{
    char str[5];

    srand(time(NULL)); //permite valores aleatórios
    int i;
    for (i = 0; i < 10; i++) //entra no laço 10 vezes, para realizar 10 cadastros
    {
        int codigoLote;
        do
        {                                 //o laço se repete até codigoLote receba um codigo único.
            codigoLote = rand() % 50 + 1; //escolhe o valor aleátorio entre 1 e 50
        } while (buscarLote(codigoLote, listaLote, *tamanhoLotePont) != -1);

        listaLote[i].codigo = codigoLote;

        stringAleatoria(str);                             //chama a função para str receber um valor aleatorio.
        strcpy(listaLote[i].nome, str);                   //coloca o valor de str como nome do lote de barris
        listaLote[i].quantidadeBarris = rand() % 50 + 50; //valor aleatório entre 50 e 99
        listaLote[i].precoCompraUnidade = rand() % 5 + 1; //valor aleatorio entre 1 e 5
        //precoVendaUnidade recebe a soma do preco da compra por unidade com um valor aleatório entre 1 e 5
        //essa soma garante que o preço da venda por unidade sempre será maior que o preco da compra por unidade
        listaLote[i].precoVendaUnidade = listaLote[i].precoCompraUnidade + rand() % 5 + 1;
        listaLote[i].codigoFornecedor = rand() % 7 + 1; //valor aleatório entre 1 e 7
        //se o codigo do Fornecedor gerado, nao tiver entre os codigos cadastrados, ele é cadastrado.
        if (buscarFornecedor(listaLote[i].codigoFornecedor, listaFornecedor, *tamanhoFornecedorPont) == -1)
        {
            listaFornecedor[*tamanhoFornecedorPont].codigo = listaLote[i].codigoFornecedor;
            stringAleatoria(str);
            strcpy(listaFornecedor[i].nome, str);
            *tamanhoFornecedorPont += 1;
        }
        *tamanhoLotePont += 1;
    }
}

//esse função preenche uma string com caracteres aleatórios
void stringAleatoria(char *str)
{
    int i;
    for (i = 0; i < 5; i++)
    {
        str[i] = 'a' + (char)(rand() % 26);
    }
}

//essa função imprime um lote de determina posição
void imprimirLote(int posicao, struct Lote *listaLote)
{
    printf("Codigo: %d\n", listaLote[posicao].codigo);
    printf("Nome do tipo: %s\n", listaLote[posicao].nome);
    printf("Quantidade de barris: %d\n", listaLote[posicao].quantidadeBarris);
    printf("Preco da compra (R$/unid): %.2f\n", listaLote[posicao].precoCompraUnidade);
    printf("Preco da venda (R$/unid): %.2f\n", listaLote[posicao].precoVendaUnidade);
    printf("Codigo do fornecedor: %d\n\n", listaLote[posicao].codigoFornecedor);
}
